# frozen_string_literal: true

require_relative './exceptions/invalid_location'

module Atoyrobot
  class Board
    MIN_X = 0
    MIN_Y = 0
    MAX_X = 5
    MAX_Y = 5

    def valid_point!(x, y)
      return if x.between?(MIN_X, MAX_X) && y.between?(MIN_Y, MAX_Y)
      raise Atoyrobot::Exceptions::InvalidLocation, "can't move to (#{x},#{y})"
    end
  end
end
