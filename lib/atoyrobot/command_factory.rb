# frozen_string_literal: true

require_relative './exceptions/invalid_command'
require_relative './location'
require_relative './commands/place'
require_relative './commands/report'
require_relative './commands/left'
require_relative './commands/move'
require_relative './commands/right'

module Atoyrobot
  class CommandFactory
    class <<self
      COMMANDS = Atoyrobot::Commands::Base.descendants_names.map(&:upcase)

      def init(command, args = [])
        valid_command(command).new(*args)
      rescue ArgumentError
        invalid_command("invalid arguments for #{command}")
      end

      private

      def valid_command(command)
        Object.const_get("Atoyrobot::Commands::#{command.capitalize}")
      rescue NameError
        invalid_command("invalid command #{command}, available are #{COMMANDS}")
      end

      def invalid_command(message)
        raise Atoyrobot::Exceptions::InvalidCommand, message
      end
    end
  end
end
