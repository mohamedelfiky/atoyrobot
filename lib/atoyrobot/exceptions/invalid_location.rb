# frozen_string_literal: true

module Atoyrobot
  module Exceptions
    class InvalidLocation < StandardError; end
  end
end
