# frozen_string_literal: true

module Atoyrobot
  module Exceptions
    class InvalidCommand < StandardError; end
  end
end
