# frozen_string_literal: true

module Atoyrobot
  module Exceptions
    class LocationUndefined < StandardError; end
  end
end
