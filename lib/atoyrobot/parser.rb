# frozen_string_literal: true

require_relative 'command_factory'

module Atoyrobot
  class Parser
    def self.parse(input)
      command, args = input.split(' ')
      CommandFactory.init(command, args&.split(',') || [])
    end
  end
end
