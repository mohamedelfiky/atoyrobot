# frozen_string_literal: true

require_relative './exceptions/location_undefined'
require 'dry-initializer'
require 'dry-types'

module Atoyrobot
  class Location
    extend Dry::Initializer

    DIRICTIONS = %w(EAST NORTH WEST SOUTH).freeze

    param :x, default: proc { nil }
    param :y, default: proc { nil }
    param :facing, default: proc { nil }

    def initialize(*args)
      super
      return if nil? || DIRICTIONS.include?(@facing)
      err = "Invalid facing direction, available are #{DIRICTIONS}"
      raise Atoyrobot::Exceptions::InvalidCommand, err
    end

    def validate!
      return unless nil?
      err = 'location undefined, run place command to set robot location'
      raise Atoyrobot::Exceptions::LocationUndefined, err
    end

    def rotate!(sign)
      validate!
      index = (DIRICTIONS.index(facing) + sign) % 4
      @facing = DIRICTIONS[index]
    end

    def report
      validate!
      [x, y, facing].join(', ')
    end

    def next_move
      validate!
      dx, dy = next_delta
      [@x + dx, @y + dy]
    end

    def set(x, y, facing = nil)
      @x = x
      @y = y
      @facing = facing unless facing.nil?
    end

    def nil?
      @x.nil? || @y.nil? || @facing.nil?
    end

    private

    def next_delta
      case facing
      when 'SOUTH' then [0, -1]
      when 'EAST' then [1, 0]
      when 'WEST' then [-1, 0]
      when 'NORTH' then [0, 1]
      end
    end
  end
end
