# frozen_string_literal: true

require 'dry-initializer'
require 'dry-types'

module Atoyrobot
  module Commands
    class Base
      extend Dry::Initializer

      def self.descendants_names
        ObjectSpace
          .each_object(Class)
          .select { |klass| klass < self }
          .map { |klass| klass.name.split('::').last }
      end

      def execute(robot)
        action = self.class.name.split('::').last.downcase
        robot.send(action)
      end
    end
  end
end
