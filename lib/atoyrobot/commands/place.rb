# frozen_string_literal: true

require_relative 'base'
require_relative '../location'
require_relative '../exceptions/invalid_command'

module Atoyrobot
  module Commands
    class Place < Base
      param :x, Dry::Types['coercible.int']
      param :y, Dry::Types['coercible.int']
      param :facing, proc { |f| f.to_s.upcase }

      def initialize(*args)
        super
        @location = Location.new(x, y, facing)
      end

      def execute(robot)
        robot.place(@location.x, @location.y, @location.facing)
      end
    end
  end
end
