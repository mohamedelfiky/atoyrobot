# frozen_string_literal: true

require_relative 'location'
require_relative 'board'

module Atoyrobot
  class Robot
    extend Dry::Initializer
    param :board, default: proc { Board.new }
    param :location, default: proc { Location.new }

    def move
      x, y = location.next_move
      board.valid_point!(x, y)
      location.set(x, y)
    end

    def left
      location.rotate!(1)
    end

    def right
      location.rotate!(-1)
    end

    def report
      puts location.report
    end

    def place(x, y, facing)
      board.valid_point!(x, y)
      location.set(x, y, facing)
    end
  end
end
