# frozen_string_literal: true

require_relative 'parser'
require_relative 'robot'

module Atoyrobot
  class CLI
    class <<self
      attr_accessor :stdin

      def start(_argv = [], args = {})
        self.stdin = args[:input] || STDIN
        robot = Robot.new
        loop do
          raw_input = stdin.gets&.chomp || ''
          break if raw_input.empty? || raw_input == 'exit'
          execute(raw_input, robot)
        end
      end

      private

      def execute(input, robot)
        command = Atoyrobot::Parser.parse(input)
        command.execute(robot)
      rescue StandardError => e
        puts e.message
      end
    end
  end
end
