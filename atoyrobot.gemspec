# coding: utf-8
# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'atoyrobot/version'

Gem::Specification.new do |spec|
  spec.name          = 'atoyrobot'
  spec.version       = Atoyrobot::VERSION
  spec.authors       = ['Mohamed Elfiky']
  spec.email         = ['mohamed.elfikyy@yahoo.com']

  spec.summary       = 'Toy Robot Simulator'
  spec.description   = 'The application is a simulation of a toy robot moving' \
                        'on a square tabletop, of dimensions 5 units x 5 units.'
  spec.homepage      = 'https://gitlab.com/mohamedelfiky/atoyrobot'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.14'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov'
  spec.add_runtime_dependency 'dry-initializer', '~> 2.4'
  spec.add_runtime_dependency 'dry-types', '~> 0.5', '> 0.5.1'
end
