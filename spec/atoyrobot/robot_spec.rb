# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/robot'

RSpec.describe Atoyrobot::Robot do
  context '#move' do
    it 'should move to the next point' do
      subject.place(0, 0, 'NORTH')
      subject.move
      expect(subject.location.x).to eq(0)
      expect(subject.location.y).to eq(1)
    end

    it 'should raise exception if the location outside the board' do
      subject.place(Atoyrobot::Board::MAX_X, Atoyrobot::Board::MAX_Y, 'NORTH')
      expect { subject.move }.to raise_error(Atoyrobot::Exceptions::InvalidLocation)
    end
  end

  context '#left' do
    it 'should delegate to location rotate with one' do
      expect(subject.location).to receive(:rotate!).with(1)
      subject.left
    end
  end

  context '#right' do
    it 'should delegate to location rotate with -1' do
      expect(subject.location).to receive(:rotate!).with(-1)
      subject.right
    end
  end

  context '#report' do
    it 'should delegate to location report' do
      expect(subject.location).to receive(:report)
      subject.report
    end

    it 'should puts location report to stdout' do
      subject.place(0, 0, 'NORTH')
      expect(STDOUT).to receive(:puts).with(subject.location.report)
      subject.report
    end
  end

  context '#place' do
    it 'should delegate to location set' do
      expect(subject.location).to receive(:set).with(0, 0, 'NORTH')
      subject.place(0, 0, 'NORTH')
    end

    it 'should raise exception if the location outside the board' do
      expect { subject.place(Atoyrobot::Board::MIN_X - 1, Atoyrobot::Board::MIN_Y - 1, 'NORTH') }
        .to raise_error(Atoyrobot::Exceptions::InvalidLocation)
    end
  end
end
