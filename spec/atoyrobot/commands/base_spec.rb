# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/commands/left'
require 'atoyrobot/commands/right'
require 'atoyrobot/commands/report'
require 'atoyrobot/commands/move'
require 'atoyrobot/robot'

RSpec.describe Atoyrobot::Commands::Base do
  context '.descendants_names' do
    let(:available_commands) { %w(Left Right Report Move Place) }

    it 'should get names of all commands' do
      expect(Atoyrobot::Commands::Base.descendants_names)
        .to match_array available_commands
    end
  end

  context '#execute' do
    let(:robot) { Atoyrobot::Robot.new }

    %w(left right report move).each do |command|
      it "should delegate #{command} command to robot" do
        valid_command = Object.const_get("Atoyrobot::Commands::#{command.capitalize}").new
        allow(robot).to receive(command) { true }
        expect(robot).to receive(command)
        valid_command.execute(robot)
      end
    end
  end
end
