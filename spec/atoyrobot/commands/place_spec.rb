# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/commands/place'
require 'atoyrobot/robot'

RSpec.describe Atoyrobot::Commands::Place do
  context '#initialize' do
    it 'should accept params if they are valid but not formated correctly' do
      place = Atoyrobot::Commands::Place.new(0, '1', 'north')
      expect(place.facing).to eq('NORTH')
      expect(place.y).to eq(1)
      expect(place.x).to eq(0)
    end
  end

  context '#execute' do
    let(:valid_command) { Atoyrobot::Commands::Place.new(0, 0, 'NORTH') }
    let(:robot) { Atoyrobot::Robot.new }

    before { allow(robot).to receive(:place) { true } }

    it 'should delegate place command to robot' do
      expect(robot).to receive(:place)
      valid_command.execute(robot)
    end
  end
end
