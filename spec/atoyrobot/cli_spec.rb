# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/cli'

RSpec.describe Atoyrobot::CLI do
  context '#run' do
    it 'should define a new robot' do
      expect(Atoyrobot::CLI).to receive(:loop)
      expect(Atoyrobot::Robot).to receive(:new)
      Atoyrobot::CLI.start
    end

    it 'should set the current stdin from params' do
      expect(Atoyrobot::CLI).to receive(:loop)
      strio = StringIO.new
      Atoyrobot::CLI.start([], input: strio)
      expect(Atoyrobot::CLI.stdin).to eq(strio)
    end

    it 'should fallback to STDIN if input = nil' do
      expect(Atoyrobot::CLI).to receive(:loop)
      Atoyrobot::CLI.start
      expect(Atoyrobot::CLI.stdin).to eq(STDIN)
    end

    it 'should run example 1' do
      example1 = File.open('./spec/atoyrobot/fixtures/example1.txt')
      expect(STDOUT).to receive(:puts).with('0, 1, NORTH')
      Atoyrobot::CLI.start([], input: example1)
    end

    it 'should run example 2' do
      example2 = File.open('./spec/atoyrobot/fixtures/example2.txt')
      expect(STDOUT).to receive(:puts).with('0, 0, WEST')
      Atoyrobot::CLI.start([], input: example2)
    end

    it 'should run example 3' do
      example3 = File.open('./spec/atoyrobot/fixtures/example3.txt')
      expect(STDOUT).to receive(:puts).with('3, 3, NORTH')
      Atoyrobot::CLI.start([], input: example3)
    end
  end

  context '#execute' do
    let(:robot) { Atoyrobot::Robot.new(Atoyrobot::Board.new, Atoyrobot::Location.new(0, 0, 'NORTH')) }
    let(:new_robot) { Atoyrobot::Robot.new }

    it 'should handle any exception and puts its message to STDOUT' do
      expect(STDOUT)
        .to receive(:puts).with('location undefined, run place command to set robot location')

      expect { Atoyrobot::CLI.send(:execute, 'move', new_robot) }
        .not_to raise_error
    end

    it 'should call parser to parse the command' do
      expect(Atoyrobot::Parser).to receive(:parse).with('move')
      Atoyrobot::CLI.send(:execute, 'move', new_robot)
    end

    it 'should call execute to the command and pass the robot object' do
      Atoyrobot::CLI.send(:execute, 'place 1,1,east', robot)
      expect(robot.location.x).to eq(1)
      expect(robot.location.y).to eq(1)
      expect(robot.location.facing).to eq('EAST')
    end
  end
end
