# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/location'

RSpec.describe Atoyrobot::Location do
  let(:valid_location) { Atoyrobot::Location.new(0, 0, 'NORTH') }
  context '#initialize' do
    it 'should raise an error if facing is not valid' do
      expect { Atoyrobot::Location.new(0, 0, 'left') }
        .to raise_error(Atoyrobot::Exceptions::InvalidCommand)
    end

    it "should accept facing if it's valid" do
      expect(valid_location.facing).to eq('NORTH')
    end
  end

  context '#validate!' do
    it "should return if location isn't nil" do
      expect { valid_location.validate! }
        .not_to raise_error
    end

    it 'should raise an exception if location is nil' do
      expect { Atoyrobot::Location.new.validate! }
        .to raise_error(Atoyrobot::Exceptions::LocationUndefined)
    end
  end

  context '#rotate!' do
    before { valid_location.set(0, 0, 'NORTH') }

    it 'should rotate the facing to right if sign is 1' do
      valid_location.rotate!(1)
      expect(valid_location.facing).to eq('WEST')
      expect(valid_location.x).to eq(0)
      expect(valid_location.y).to eq(0)
    end

    it 'should rotate the facing to left if sign is -1' do
      valid_location.rotate!(-1)
      expect(valid_location.facing).to eq('EAST')
      expect(valid_location.x).to eq(0)
      expect(valid_location.y).to eq(0)
    end

    it "should return if location isn't nil" do
      expect { Atoyrobot::Location.new.validate! }
        .to raise_error(Atoyrobot::Exceptions::LocationUndefined)
    end
  end

  context '#report' do
    it 'should report the location formated' do
      expect(valid_location.report).to eq('0, 0, NORTH')
    end

    it "should return if location isn't nil" do
      expect { Atoyrobot::Location.new.validate! }
        .to raise_error(Atoyrobot::Exceptions::LocationUndefined)
    end
  end

  context '#next_move' do
    it 'should return the next point by adding the delta and the current x,y' do
      allow(valid_location).to receive(:next_delta) { [1, 1] }
      expect(valid_location).to receive(:next_delta)
      expect(valid_location.next_move).to eq([1, 1])
    end

    it 'should return the next point not changing the current loaction x,y' do
      expect { valid_location.next_move }
        .to change { valid_location.y }.by(0)
      expect { valid_location.next_move }
        .to change { valid_location.x }.by(0)
    end

    it "should return if location isn't nil" do
      expect { Atoyrobot::Location.new.validate! }
        .to raise_error(Atoyrobot::Exceptions::LocationUndefined)
    end
  end

  context '#set' do
    it 'should set the location x, y, facing' do
      valid_location.set(1, 2, 'EAST')
      expect(valid_location.x).to eq(1)
      expect(valid_location.y).to eq(2)
      expect(valid_location.facing).to eq('EAST')
    end

    it 'should set location x,y and ignore facing if nil' do
      old_facing = valid_location.facing
      valid_location.set(1, 2)
      expect(valid_location.x).to eq(1)
      expect(valid_location.y).to eq(2)
      expect(valid_location.facing).to eq(old_facing)
    end
  end

  context '#next_delta' do
    it 'should get the next delta for north' do
      valid_location.set(1, 2, 'NORTH')
      expect(valid_location.send(:next_delta)).to eq([0, 1])
    end

    it 'should get the next delta for west' do
      valid_location.set(1, 2, 'WEST')
      expect(valid_location.send(:next_delta)).to eq([-1, 0])
    end

    it 'should get the next delta for east' do
      valid_location.set(1, 2, 'EAST')
      expect(valid_location.send(:next_delta)).to eq([1, 0])
    end

    it 'should get the next delta for south' do
      valid_location.set(1, 2, 'SOUTH')
      expect(valid_location.send(:next_delta)).to eq([0, -1])
    end
  end

  context '#nil?' do
    it 'return true if x || y || facing is nil' do
      expect(Atoyrobot::Location.new.nil?).to eq(true)
    end

    it 'return false if x, y, facing is not nil' do
      expect(valid_location.nil?).to eq(false)
    end
  end
end
