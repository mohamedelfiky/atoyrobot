# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/command_factory'
require 'atoyrobot/exceptions/invalid_command'

RSpec.describe Atoyrobot::CommandFactory do
  context '#valid_command' do
    it 'should accept commands with upcase letters' do
      expect(Atoyrobot::CommandFactory.send(:valid_command, 'MOVE'))
        .to eq(Atoyrobot::Commands::Move)
    end

    it 'should accept commands with downcase letters' do
      expect(Atoyrobot::CommandFactory.send(:valid_command, 'move'))
        .to eq(Atoyrobot::Commands::Move)
    end

    it 'should raise exception if command not valid' do
      expect { Atoyrobot::CommandFactory.send(:valid_command, 'sing') }
        .to raise_error(Atoyrobot::Exceptions::InvalidCommand)
    end
  end

  context '#init' do
    it 'should raise exception if passed invalid number args to a command' do
      expect { Atoyrobot::CommandFactory.init('PLACE', [0, 0]) }
        .to raise_error(Atoyrobot::Exceptions::InvalidCommand)
    end

    it 'should raise exception if passed invalid args to a command' do
      expect { Atoyrobot::CommandFactory.init('PLACE', [0, 'x', 'north']) }
        .to raise_error(Atoyrobot::Exceptions::InvalidCommand)
    end
  end

  context '#invalid_command' do
    it 'should raise exception invalid command' do
      expect { Atoyrobot::CommandFactory.send(:invalid_command, 'message') }
        .to raise_error(Atoyrobot::Exceptions::InvalidCommand)
    end
  end
end
