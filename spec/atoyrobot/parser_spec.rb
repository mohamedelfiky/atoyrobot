# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/parser'

RSpec.describe Atoyrobot::Parser do
  context '#parse' do
    it 'should parse the input and extract paramters form it' do
      allow(Atoyrobot::CommandFactory).to receive(:init) { true }
      expect(Atoyrobot::CommandFactory).to receive(:init).with('PLACE', %w(0 1 EAST))
      Atoyrobot::Parser.parse('PLACE 0,1,EAST')
    end

    it 'should ignore args if one word command' do
      allow(Atoyrobot::CommandFactory).to receive(:init) { true }
      expect(Atoyrobot::CommandFactory).to receive(:init).with('REPORT', [])
      Atoyrobot::Parser.parse('REPORT')
    end

    it "should return an object from command if it's a valid command" do
      expect(Atoyrobot::Parser.parse('REPORT')).to be_kind_of(Atoyrobot::Commands::Report)
    end
  end
end
