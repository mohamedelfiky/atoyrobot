# frozen_string_literal: true

require 'spec_helper'
require 'atoyrobot/board'
require 'atoyrobot/exceptions/invalid_location'

describe Atoyrobot::Board do
  subject { Atoyrobot::Board }
  let(:board) { subject.new }

  context '#valid_point!' do
    it 'should not to raise exception if point is in board' do
      expect { board.valid_point!(subject::MIN_X, subject::MIN_Y) }
        .not_to raise_error
    end

    it 'should raise exception if point is out of board' do
      expect { board.valid_point!(subject::MIN_X - 1, subject::MIN_Y - 2) }
        .to raise_error(Atoyrobot::Exceptions::InvalidLocation)

      expect { board.valid_point!(subject::MAX_X + 1, subject::MAX_Y + 2) }
        .to raise_error(Atoyrobot::Exceptions::InvalidLocation)
    end
  end
end
